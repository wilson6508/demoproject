FROM openjdk:8-jdk-alpine
COPY ./target/DemoProject-1.0.jar /tmp/app.jar
EXPOSE 8080
ENTRYPOINT java -jar /tmp/app.jar

# FROM maven:3.8.5-openjdk-11 AS maven_build
# COPY pom.xml /tmp/
# COPY src /tmp/src/
# WORKDIR /tmp/
# RUN mvn package
# EXPOSE 8080
# CMD ["java", "-jar", "target/DemoProject-1.0.jar"]