package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/hello")
    public String hello() {
        return "hello 123";
    }

    @GetMapping("/demo")
    public String demo() {
        return "demo 456";
    }    

}
